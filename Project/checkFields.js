"use strict";

document.addEventListener("DOMContentLoaded", function () {
  const startGameButton = document.getElementById("start-game");
  const submitButton = document.getElementById("submit");
  const playerNameInput = document.getElementById("playerName");
  const numberChoiceInput = document.getElementById("numberChoice"); // Corrected the typo here

  // Initialize a flag to track the completion status
  let allFieldsCompleted = false;

  // Add input event listeners for player name, color choice, and number choice
  playerNameInput.addEventListener("input", checkFields);
  numberChoiceInput.addEventListener("input", checkFields);
  
  // Check the completion status
  function checkFields() {
    const playerName = playerNameInput.value;
    const numberChoice = numberChoiceInput.value;

    // Update the completion status flag
    allFieldsCompleted = playerName && !isNaN(numberChoice) && numberChoice >= 3 && numberChoice < 7;

    // Enable or disable the "Start the game!" button based on the completion status
    startGameButton.disabled = !allFieldsCompleted;
  }
  
  startGameButton.addEventListener("click", () => {
	  if(allFieldsCompleted) {
		  startGameButton.disabled = true;
	  }
	  submitButton.disabled = false;
	  
  });
  
});
