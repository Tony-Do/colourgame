"use strict";

document.addEventListener("DOMContentLoaded", function () {
  const numberChoice = document.getElementById("numberChoice");
  const gameBoard = document.querySelector(".game-board");
  const startGameButton = document.getElementById("start-game");
  
  if (gameBoard) {
    // Add a click event listener for the "Start the game!" button
    startGameButton.addEventListener("click", function () {
      createTable();
    });
  }

  function createTable() {
    const number = parseInt(numberChoice.value);
    if (isNaN(number)) {
      return;
    }

    // Clear the existing table
    gameBoard.innerHTML = "";

    // Create a new table
    const table = document.createElement("table");

    for (let i = 0; i < number; i++) {
      const row = document.createElement("tr");

      for (let j = 0; j < number; j++) {
        const cell = document.createElement("td");
        row.appendChild(cell);
      }

      table.appendChild(row);
    }
    gameBoard.appendChild(table);
	
	/* Randomizes the rgb */
	const tiles = document.querySelectorAll(".game-board td");
	
	tiles.forEach(function (tile) {
		
		const red = Math.floor(Math.random() * 256);
		const green = Math.floor(Math.random() * 256);
		const blue = Math.floor(Math.random() * 256);
		
		const textDiv = document.createElement("div");
		textDiv.textContent = `rgb: (${red}, ${green}, ${blue})`;
		tile.appendChild(textDiv);
		tile.style.backgroundColor = `rgb(${red},${green},${blue})`; 
		
		
	}); 
	
	/* Cheat code Shift + C */
	document.addEventListener("keydown", function (e) {
		if(e.shiftKey && e.key === "C") {
			toggleTileTextVisibility();
		}
	});
	let tilesHidden = true;
	function toggleTileTextVisibility() {
		const tileTexts = document.querySelectorAll(".game-board td div");
	  
		tileTexts.forEach(function (textDiv) {
		textDiv.style.visibility = tilesHidden ? "visible" : "hidden";
	  });
	  tilesHidden = !tilesHidden;
	}


	
  }
  
});
