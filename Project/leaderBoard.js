"use strict";

document.addEventListener("DOMContentLoaded", function () {
  const playerNameInput = document.getElementById("playerName");
  const leaderboard = document.querySelector(".leader-board");
  const startGameButton = document.getElementById("start-game");
  startGameButton.addEventListener("click", addPlayerToLeaderboard);

  function addPlayerToLeaderboard() {
    const playerName = playerNameInput.value;

    if (playerName.trim() !== "") {
      const row = document.createElement("tr");
      const playerNameCell = document.createElement("td");
      playerNameCell.textContent = playerName;
      const scoreCell = document.createElement("td");
      scoreCell.textContent = ""; // Replace with actual scores
      row.appendChild(playerNameCell);
      row.appendChild(scoreCell);
      leaderboard.querySelector("table").appendChild(row);
      playerNameInput.value = ""; // Clear the input field
    }
  }
  
});
